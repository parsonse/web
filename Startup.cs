using CovidQuestionnaire.DAL;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Net.Http.Headers;
using System;
using Microsoft.AspNetCore.Mvc;
using CovidQuestionnaire.Infrastructure;
using CovidQuestionnaire.Services;

namespace CovidQuestionnaire
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews(options =>
            {
                options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
                options.CacheProfiles.Add("DefaultDay",
                    new CacheProfile()
                    {
                        Duration = 31536000,
                        Location = ResponseCacheLocation.Any,
                        NoStore = false
                    });
            });


            
            services.AddConfig<WellnessConfig>(Configuration.GetSection("Wellness"));
            services.AddScoped<IDatabaseContext>(_ => new DatabaseContext(Configuration.GetConnectionString("CovidDBContext")));
            services.AddScoped<IRepo, Repo>();
            services.AddScoped<IEmailService, EmailService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles(new StaticFileOptions
            {
                OnPrepareResponse = ctx =>
                {

                    var dt = DateTime.Now;
                    dt = dt.AddDays(365);
                    ctx.Context.Response.Headers[HeaderNames.CacheControl] =
                        "public,max-age=" + 31536000;
                    ctx.Context.Response.Headers[HeaderNames.Expires] = dt.ToShortDateString();
                }
            });

            app.UseXXssProtection(options => options.EnabledWithBlockMode());

            app.UseXContentTypeOptions();

            app.UseReferrerPolicy(opts => opts.NoReferrer());

            app.UseXfo(options => options.Deny());

            app.UseHsts(options => options.MaxAge(days: 30).IncludeSubdomains());


            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
