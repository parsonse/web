﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CovidQuestionnaire.Infrastructure
{
    public class WellnessConfig
    {
        public Settings Settings { get; set; } = new Settings();
        public Contacts Contacts { get; set; } = new Contacts();
        public EmailConfiguration Email { get; set; } = new EmailConfiguration();
    }


    public class Settings {
        public string Domain { get; set; }
    }
    public class Contacts {
        public string AFFEmails { get; set; }
        public string MonsterEmails { get; set; }
    }
    public class EmailConfiguration
    {
        public string SMTP { get; set; } = "smtp.sendgrid.net";
        public string From { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string CustomerService { get; set; }
    }
}
