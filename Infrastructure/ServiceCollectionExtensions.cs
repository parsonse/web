﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;

namespace CovidQuestionnaire.Infrastructure
{
    public static class ServiceCollectionExtensions
    {
        public static TItem AddConfig<TItem>(this IServiceCollection services, IConfiguration configuration)
                where TItem : class, new()
        {
            return services.AddConfig<TItem>(configuration, options => { });
        }

        public static TItem AddConfig<TItem>(this IServiceCollection services, IConfiguration configuration, Action<BinderOptions> configureOptions)
            where TItem : class, new()
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            TItem setting = configuration.Get<TItem>(configureOptions);
            services.TryAddSingleton(setting);
            return setting;
        }
    }
}
