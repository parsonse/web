﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CovidQuestionnaire.Models
{
    public class Form
    {
        public string EmployeeId { get; set; }
        public int DepartmentId { get; set; }
        public string  DepartmentName { get; set; }
        public string Supervisor { get; set; }
        public bool Question1 { get; set; }
        public string Question2 { get; set; }
        public string Question3 { get; set; }
        public string Question3Details { get; set; }
        public bool Question4 { get; set; }
        public string Signature { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

    }
}
