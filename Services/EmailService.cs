﻿using CovidQuestionnaire.Infrastructure;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace CovidQuestionnaire.Services
{
    public interface IEmailService
    {
        Task<bool> SendEmail(string subject, string body, string toEmail, string fromEmail);
    }
    public class EmailService : IEmailService
    {
        private WellnessConfig _config;
        public EmailService(WellnessConfig config) => (_config) = (config);
        public async Task<bool> SendEmail(string subject, string body, string toEmail, string fromEmail)
        {

            var success = false;
            try
            {
                SmtpClient smtpClient = new SmtpClient()
                {
                    Host = _config.Email.SMTP,
                    Port = 587,
                    Credentials = new NetworkCredential(
                            _config.Email.Username,
                            _config.Email.Password
                        )
                };

                var msg = new MailMessage();
                msg.Subject = subject;
                msg.Body = body;
                msg.IsBodyHtml = true;

                foreach (var email in toEmail.Split(','))
                {
                    msg.To.Add(email);
                }

                MailAddress addrFrom = new MailAddress(fromEmail);
                msg.From = addrFrom;

                await smtpClient.SendMailAsync(msg);

                success = true;
            }
            catch (Exception ex)
            {
                success = false;
            }

            return success;
        }
    }
}
