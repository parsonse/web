﻿using CovidQuestionnaire.DAL;
using CovidQuestionnaire.Infrastructure;
using CovidQuestionnaire.Models;
using CovidQuestionnaire.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using System.Diagnostics;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;

namespace CovidQuestionnaire.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRepo _repo;
        private readonly WellnessConfig _config;
        readonly ICompositeViewEngine _viewEngine;
        readonly IEmailService _email;


        public HomeController(IRepo repo, ICompositeViewEngine viewEngine, WellnessConfig config, IEmailService email)
        {
            _repo = repo;
            _config = config;
            _viewEngine = viewEngine;
            _email = email;
        }

        public async Task<IActionResult> Index()
        {
            return View(new HomeViewModel
            {
                Departments = await _repo.GetDepartments()
            });
        }

        [Route("ModernaCheckIn")]
        public IActionResult Moderna()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> SubmitModerna()
        {

            var success = false;
            var message = "";
            try
            {
                Form details = new Form();
                details = JsonSerializer.Deserialize<Form>(Request.Form["details"]);
                var view = _viewEngine.FindView(ControllerContext, "CheckInEmail", false).View;
                var writer = new StringWriter();

                ViewData["Domain"] = _config.Settings.Domain;
                ViewData["Form"] = details;

                var viewContext = new ViewContext(ControllerContext, view, ViewData, TempData, writer, new HtmlHelperOptions());

                await view.RenderAsync(viewContext);

                await _email.SendEmail("Monster Wellness Check", writer.GetStringBuilder().ToString(), _config.Contacts.MonsterEmails, _config.Email.From);
                message = "good";
                success = true;
            }
            catch (Exception ex)
            {
                success = false;
                message = ex.Message;
            }

            return Json(new { success, message });
        }

        [HttpGet]
        public async Task<IActionResult> CheckUserName(string userName)
        {
            try
            {
                return Ok(new { userId = await _repo.CheckUserName(userName) });
            }
            catch (Exception e) 
            { 
                return Ok(new { userId = 0 }); 
            }
            
        }

        [HttpPost]
        public async Task<IActionResult> SubmitDetails()
        {

            var success = false;
            var message = "";
            try
            {
                Form details = new Form();
                details = JsonSerializer.Deserialize<Form>(Request.Form["details"]);
                await Task.Run(() => _repo.InsertForm(details));
                success = true;

                if (details.Question1 == true || details.Question2 == "true" || details.Question3 == "true" || details.Question4 == false)
                //if (details.Question1 == true || details.Question2 == "true" || details.Question3 == "true" || details.Question4 == false || details.Question2 == "n/a" || details.Question3 == "n/a")
                {

                    var view = _viewEngine.FindView(ControllerContext, "Email", false).View;
                    var writer = new StringWriter();


                    try
                    {
                        ViewData["Domain"] = _config.Settings.Domain;
                        ViewData["Form"] = details;


                        var viewContext = new ViewContext(ControllerContext, view, ViewData, TempData, writer, new HtmlHelperOptions());

                        await view.RenderAsync(viewContext);

                        string email = details.DepartmentName.StartsWith("AFF") ? _config.Contacts.AFFEmails : _config.Contacts.MonsterEmails;
                        await _email.SendEmail("Monster Wellness Check", writer.GetStringBuilder().ToString(), email, _config.Email.From);

                        //if (details.Question2 == "n/a" || details.Question3 == "n/a")
                        //{
                        //    message = "good";
                        //}
                        //else
                        //{
                            message = "bad";
                       // }

                    }
                    catch(Exception ex)
                    {
                        throw ex;
                    }
                }
                else {
                    message = "good";
                }
            }
            catch (Exception ex)
            {
                success = false;
                message = ex.Message;
            }

            return Json(new { success, message });
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Terms()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
