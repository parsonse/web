﻿using CovidQuestionnaire.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using System.Data;

namespace CovidQuestionnaire.DAL
{
    public interface IRepo {
        Task InsertForm(Form form);
        Task<IEnumerable<Department>> GetDepartments();
        Task<int> CheckUserName(string UserName);
    }
    public class Repo : IRepo
    {
        private readonly IDatabaseContext _db;
        public Repo(IDatabaseContext db) {
            _db = db;
        }

        public async Task InsertForm(Form form)
        {
            try
            {

   
                await _db.Connection.ExecuteAsync("[InsertSubmission]", new
                {
                     EmployeeId = form.EmployeeId,
                     DepartmentId =form.DepartmentId,
                     Supervisor = form.Supervisor,
                     Question1 = form.Question1,
                     Question2 = form.Question2 == "n/a" ? null : form.Question2,
                     Question3 = form.Question3 == "n/a" ? null : form.Question3,
                     Question3Details = form.Question3Details,
                     Question4 = form.Question4,
                     Signature = form.Signature,

                 }, commandType: CommandType.StoredProcedure);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                _db.Connection.Close();
            }
        }

        public async Task<IEnumerable<Department>> GetDepartments()
        {
            try
            {
               return await _db.Connection.QueryAsync<Department>("[GetDepartments]", commandType: CommandType.StoredProcedure);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                _db.Connection.Close();
            }
        }

        public async Task<int> CheckUserName(string UserName)
        {
            try
            {
                return await _db.Connection.QueryFirstOrDefaultAsync<int>("[CheckUserName]", new { UserName = UserName}, commandType: CommandType.StoredProcedure);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                _db.Connection.Close();
            }
        }

    }
}
