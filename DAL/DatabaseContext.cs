﻿using MicroOrm.Dapper.Repositories.DbContext;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;


namespace CovidQuestionnaire.DAL
{
    public interface IDatabaseContext: IDapperDbContext { }
    public class DatabaseContext : DapperDbContext, IDatabaseContext
    {

        public DatabaseContext(string connectionString)
            : base(new SqlConnection(connectionString))
        {
        }
    }
}


