﻿var main = function () {
    return {
        init: function () {

            var forms = document.getElementsByClassName('needs-validation-1');
            $('#employee-id').focus(function () {
                $('.bad-user-id').hide();
            });
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    event.preventDefault();
                    event.stopPropagation();

                    $.get({
                        url: '/Home/CheckUserName',
                        data: { userName: $('#employee-id').val() }
                    }).done(function (response) {
                        if (response.userId === 0 && Number($('#is-employee').val()) === 1) {
                            $('.bad-user-id').fadeIn();
                            $('#questions').hide();
                        }
                        else {
                            if (form.checkValidity() === true && $('.bad-user-id').is(":visible") === false) {

                                if ($('#questions:visible').length === 0) {

                                    $('#questions').show();
                                    $('html, body').animate({
                                        scrollTop: $("#form-start").offset().top - 50
                                    }, 300);

                                    var forms1 = document.getElementsByClassName('needs-validation-2');
                                    // Loop over them and prevent submission
                                    var validation = Array.prototype.filter.call(forms1, function (form) {
                                        form.addEventListener('submit', function (event) {
                                            if ($('#q3-yes').prop('checked') === true) {
                                                $('#q3-details').prop('required', true);
                                            }
                                            else {
                                                $('#q3-details').prop('required', false);
                                            }
                                            event.preventDefault();
                                            event.stopPropagation();

                                            if (form.checkValidity() === true) {
                                                main.submitForm();
                                                $('.failure').hide();
                                            }


                                            form.classList.add('was-validated');
                                        }, false);
                                    });
                                }

                            }


                            form.classList.add('was-validated');
                        }
                    });

                    
                }, false);
            });
        },
        checkUserName: function () {
         
            
            
        },
        submitForm: function () {
            var frm = new FormData();

            var q2Answer, q3Answer;
            if ($('#q2-yes').prop('checked') === true) {
                q2Answer = 'true';
            }
            else if ($('#q2-no').prop('checked') === true) {
                q2Answer = 'false';
            }
            else {
                q2Answer = 'n/a';
            }

            if ($('#q3-yes').prop('checked') === true) {
                q3Answer = 'true';
            }
            else if ($('#q3-no').prop('checked') === true) {
                q3Answer = 'false';
            }
            else {
                q3Answer = 'n/a';
            }


            var frmData = new main.form(
                $('#employee-id').val(),
                $('#department').val(),
                $('#department  option:selected').text(),
                $('#employee-supervisor').val(),
                $('#q1-yes').prop('checked') === true ? true : false,
                q2Answer,
                q3Answer,
                $('#q3-details').val(),
                $('#q4-yes').prop('checked') === true ? true : false,
                $('#signature').val()
            );

            frm.append("details", JSON.stringify(frmData));
            frm.append('__RequestVerificationToken', $('[name=__RequestVerificationToken]').val());

            $.ajax({
                url: '/Home/SubmitDetails',
                type: 'POST',
                data: frm,
                processData: false,
                contentType: false,
                success: function (response) {
                    if (response.success) {
                        $('.needs-validation-2 button').prop('disabled', true);
                        $('.success').fadeIn();
                        if (response.message === "good") {
                            var x = document.getElementById("goodResponse"); 
                            x.play(); 
                        }
                        if (response.message === "bad") {
                            var x = document.getElementById("badResponse"); 
                            x.play(); 
                        }
                        $('html, body').animate({
                            scrollTop: $(".success").offset().top
                        }, 300);

                        setTimeout(function () { location.reload() }, 4000)
                    }
                    else {
                        alert('There was an error saving your response. Error: ' + response.message);
                    }
                }
            });
        },

        form: function (EmployeeId, DepartmentId, DepartmentName, Supervisor, Question1, Question2, Question3, Question3Details, Question4, Signature) {
            this.EmployeeId = EmployeeId;
            this.DepartmentId = Number(DepartmentId);
            this.DepartmentName = DepartmentName;
            this.Supervisor = Supervisor;
            this.Question1 = Boolean(Question1);
            this.Question2 = Question2;
            this.Question3 = Question3;
            this.Question3Details = Question3Details;
            this.Question4 = Boolean(Question4);
            this.Signature = Signature;
        }
    };
}();

var moderna = function () {
    return {
        init: function () {
            var forms = document.getElementsByClassName('needs-validation');
            var validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    event.preventDefault();
                    event.stopPropagation();

                    if (form.checkValidity() === true) {                  

                        moderna.submitForm();
                       
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        },

        submitForm: function () {
            var frm = new FormData();
            var frmData = new moderna.form(
                $('#firstName').val(),
                $('#lastName').val(),
                $('#department').val()
            );

            frm.append("details", JSON.stringify(frmData));
            frm.append('__RequestVerificationToken', $('[name=__RequestVerificationToken]').val());

            $.ajax({
                url: '/Home/SubmitModerna',
                type: 'POST',
                data: frm,
                processData: false,
                contentType: false,
                success: function (response) {
                    if (response.success) {
                        $('.success').fadeIn();

                        setTimeout(function () { location.reload() }, 4000)
                    }
                    else {
                        alert('There was an error saving your response. Error: ' + response.message);
                    }
                }
            });
        },

        form: function (firstName, lastName, department) {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.DepartmentName = department;
        }
    };
}();